#ifndef SERVER_H
#define SERVER_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Запуск сервера.
//! \param addr Адрес сетевого интерфейса.
//! \param port Порт.
//! \return Код ошибки: 0, если сервер закончил работу без ошибок.
//!
int ServerStart(const char* addr, uint16_t port);

#ifdef __cplusplus
}
#endif

#endif // SERVER_H
