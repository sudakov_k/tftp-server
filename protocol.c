#include <string.h>
#include <ctype.h>
#include "protocol.h"

// Преобразование данных в тип uint16_t
static uint16_t toU16(char high, char low)
{
    // Преобразование char -> unsigned char необходимо
    // для избежания ошибок преобразования отрицательных чисел
    uint16_t value = (unsigned char) low;
    value |= (((uint16_t) ((unsigned char) high)) << 8) & 0xFF00;

    return value;
}

// Преобразование типа uint16_t в массив данных
// Размер массива: 2 байта
static void fromU16(char *buf, uint16_t value)
{
    if (buf) {
        buf[0] = (value >> 8) & 0xFF;
        buf[1] = value & 0xFF;
    }
}

// Сравнение строк без учета регистра
static bool strCmpI(const char *str1, const char *str2)
{
    bool isEq = false;

    if (str1 && str2) {
        const char *it1 = str1;
        const char *it2 = str2;

        // Посимвольное сравнение, если символы различаются,
        // итераторы укажут на место, где было закончено
        // сравнение
        while ((*it1 != '\0') && (*it2 != '\0')) {
            if (tolower(*it1) != tolower(*it2)) {
                break;
            }

            ++it1;
            ++it2;
        }

        // Проверка на одинаковую длинну строк
        if ((*it1 == '\0') && (*it2 == '\0')) {
            isEq = true;
        }
    }

    return isEq;
}

// Возвращает true, если переданное значение является кодом ошибки
static bool tftpIsErrorCode(tftp_error_t err)
{
    bool isOk = false;

    switch (err) {
    case TFTP_ERROR_NDEF:
    case TFTP_ERROR_NFOUND:
    case TFTP_ERROR_ACCESS:
    case TFTP_ERROR_ALLOC:
    case TFTP_ERROR_ILLEGAL:
    case TFTP_ERROR_ID:
    case TFTP_ERROR_FEXIST:
    case TFTP_ERROR_USER:
        isOk = true;
        break;

    default:
        break;
    }

    return isOk;
}

bool TftpInfo(const char *buf, size_t bufSize, tftp_message_t *message)
{
    bool isOk = false;

    // Минимальный размер для всех сообщений - 4 байта
    if (buf && message && (bufSize >= 4)) {
        tftp_message_t tmp = toU16(buf[0], buf[1]);

        switch (tmp) {
        case TFTP_RRQ:
        case TFTP_WRQ:
        case TFTP_DATA:
        case TFTP_ACK:
        case TFTP_ERR:
            *message = tmp;
            isOk = true;
            break;

        default:
            break;
        }
    }

    return isOk;
}

bool TftpParseRW(const char *buf, size_t bufSize, const char **fileName, tftp_mode_t *mode)
{
    bool isOk = false;

    // Минимальный размер RRQ и WRQ - 8 байт (протокол - mail, имя файла имеет 0 символов)
    if (buf && (bufSize >= 8)) {
        tftp_message_t message = toU16(buf[0], buf[1]);

        if ((message == TFTP_RRQ) || (message == TFTP_WRQ)) {
            // Максимальный размер строки в сообщении (c учетом '\0')
            // сейчас считается, что сообщение может не содержать
            // строки передачи данных
            size_t strMax = bufSize - 2;

            // Указатель на начало строки
            const char *str = buf + 2;

            // Размер строки имени файла
            size_t strFileSize = strnlen(str, strMax);

            if ((strFileSize > 0) && (strFileSize < strMax)) {
                // Максимальный размер режима передачи (c учетом '\0')
                strMax -= strFileSize + 1;

                if (strMax > 0) {
                    // Смещение:
                    // 2 байта - команда;
                    // strFileSize - имя файла;
                    // 1 байт - конец строки имени файла.
                    str += strFileSize + 1;

                    size_t strModeSize = strnlen(str, strMax);

                    // Последний гарантированно является концом строки,
                    // иначе strModeSize было бы равно strMax.
                    // Также гарантированно достигнут конец сообщения.
                    if (strModeSize == (strMax - 1)) {
                        tftp_mode_t tftpMode = TFTP_MODE_ERROR;

                        if (strCmpI(str, "netascii")) {
                            // NETASCII
                            tftpMode = TFTP_MODE_NETASCII;
                        } else if (strCmpI(str, "octet")) {
                            // OCTET
                            tftpMode = TFTP_MODE_OCTET;
                        }

                        /* Устаревший режим
                        else if (strCmpI(str, "mail")) {
                            // MAIL
                            tftpMode = TFTP_MODE_MAIL;
                        }
                        */

                        if (tftpMode != TFTP_MODE_ERROR) {
                            // Разбор успешно завершен, посылка корректна
                            isOk = true;

                            // Копирование полей сообщения

                            // Имя файла
                            if (fileName) {
                                *fileName = buf + 2;
                            }

                            // Режим передачи
                            if (mode) {
                                *mode = tftpMode;
                            }
                        }
                    }
                }
            }
        }
    }

    return isOk;
}

bool TftpParseAck(const char *buf, size_t bufSize, uint16_t *i)
{
    bool isOk = false;

    if (buf && (bufSize == 4)) {
        if (toU16(buf[0], buf[1]) == TFTP_ACK) {
            isOk = true;

            if (i) {
                *i = toU16(buf[2], buf[3]);
            }
        }
    }

    return isOk;
}

bool TftpParseErr(const char *buf, size_t bufSize, tftp_error_t *errCode, char *errStr, size_t *strSize)
{
    bool isOk = false;

    // Минимальный размер ERR - 5 байт (строка с описанием ошибки имеет 0 символов)
    if (buf && (bufSize >= 5)) {
        if (toU16(buf[0], buf[1]) == TFTP_ERR) {
            // Максимальный размер строки в сообщении (c учетом '\0')
            size_t msgStrMax = bufSize - 4;

            // Размер строки имени файла
            size_t msgStrSize = strnlen(buf + 4, msgStrMax);

            // Если условия выполняется - посылка корректна,
            // иначе - строка сообщения содержит нули
            // или последный байт посылки не равен 0
            if (msgStrSize == (msgStrMax - 1)) {
                tftp_error_t err = toU16(buf[2], buf[3]);
                if (tftpIsErrorCode(err)) {
                    isOk = true;

                    // Запись кода ошибки
                    if (errCode) {
                        *errCode = err;
                    }

                    // Запись требуемого размера строки
                    if (strSize) {
                        // Запись строки
                        if (errStr && (*strSize > msgStrSize)) {
                            strcpy(errStr, buf + 4);
                        }

                        *strSize = msgStrSize + 1;
                    }
                }
            }
        }
    }

    return isOk;
}

bool TftpParseData(const char *buf, size_t bufSize, uint16_t *id, const char **data, size_t *dataSize)
{
    bool isOk = false;

    if (buf && (bufSize >= 4)) {
        if (toU16(buf[0], buf[1]) == TFTP_DATA) {
            isOk = true;

            if (id) {
                *id = toU16(buf[2], buf[3]);
            }

            if (data) {
                *data = buf + 4;
            }

            if (dataSize) {
                *dataSize = bufSize - 4;
            }
        }
    }

    return isOk;
}

size_t TftpData(char *outBuf, size_t outSize, uint16_t i, const char *inBuf, size_t inSize)
{
    size_t size = inSize + 4;

    if (outBuf && (outSize >= size)) {
        fromU16(outBuf, TFTP_DATA); // Тип сообщения
        fromU16(outBuf + 2, i);     // Номер посылки

        if (inBuf && (inSize > 0)) {
            // Блок данных
            memcpy(outBuf + 4, inBuf, inSize);
        }
    }

    return size;
}

size_t TftpAck(char *outBuf, size_t outSize, uint16_t i)
{
    size_t size = 4;

    if (outBuf && (outSize >= size)) {
        fromU16(outBuf, TFTP_ACK);  // Тип сообщения
        fromU16(outBuf + 2, i);     // Номер посылки
    }

    return size;
}

size_t TftpErr(char *outBuf, size_t outSize, tftp_error_t err, const char *str)
{
    size_t size = 5 + strlen(str);

    if (outBuf && (outSize >= size)) {
        fromU16(outBuf, TFTP_ERR);  // Тип сообщения
        fromU16(outBuf + 2, err);   // Код ошибки
        strcpy(outBuf + 4, str);    // Текст ошибки
    }

    return size;
}
