#include <stdlib.h>
#include <string.h>
#include "thread_array.h"

thread_array_t * ThreadArrayInit(size_t maxSize)
{
    // Разметка памяти для структуры
    thread_array_t *array = malloc(sizeof(thread_array_t));

    if (array) {
        // Разметка памяти для данных
        array->data = malloc(maxSize * sizeof(pthread_t));

        if (array->data) {
            array->size = 0;
            array->maxSize = maxSize;
        } else {
            // Ошибка разметки памяти,
            // очистка ресурсов
            free(array);
            array = NULL;
        }
    }

    return array;
}

void ThreadArrayFree(thread_array_t *array)
{
    if (array) {
        free(array->data);
        free(array);
    }
}

size_t ThreadArraySize(thread_array_t *array)
{
    return array ? array->size : 0;
}

bool ThreadArrayFull(thread_array_t *array)
{
    return array ? (array->size >= array->maxSize) : true;
}

pthread_t ThreadArrayData(thread_array_t *array, size_t i)
{
    pthread_t data = 0;

    if (array) {
        if (i < array->size) {
            data = array->data[i];
        }
    }

    return data;
}

bool ThreadArrayAppend(thread_array_t *array, pthread_t value)
{
    bool isOk = false;

    if (array) {
        if (array->size < array->maxSize) {
            array->data[array->size] = value;
            ++array->size;
            isOk = true;
        }
    }

    return isOk;
}

bool ThreadArrayRemove(thread_array_t *array, pthread_t value)
{
    bool isOk = false;

    if (array) {
        for (size_t i = 0; i < array->size; ++i) {
            if (array->data[i] == value) {
                --array->size;
                memmove(array + i, array + i + 1, (array->size - i) * sizeof(pthread_t));
                isOk = true;
            }
        }
    }

    return isOk;
}
