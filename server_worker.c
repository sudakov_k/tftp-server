#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "server_worker.h"
#include "server_worker_rw.h"
#include "server_transfer.h"

worker_pipe_t * WorkerPipeInit()
{
    worker_pipe_t *workerPipe = malloc(sizeof(worker_pipe_t));

    if (workerPipe) {
        // Инициализация мьютекса
        if (pthread_mutex_init(&workerPipe->writeLock, NULL) == 0) {
            bool pipeInit = false;  // Успех инициализации канала
            int fd[2];              // Файловые дескрипторы канала

            // Канал для передачи главному потоку информации о дочерних потоках,
            // которые завершили свою работу
            if (pipe(fd) == 0) {
                // Установка неблокирующего режима для канала (только для чтения)
                int flags = fcntl(fd[0], F_GETFL, 0);

                if (flags != -1) {
                    flags |= O_NONBLOCK;

                    if (fcntl(fd[0], F_SETFL, flags) == 0) {
                        // Переключение успешно завершено
                        workerPipe->readFd = fd[0];
                        workerPipe->writeFd = fd[1];
                        workerPipe->readSize = 0;
                        pipeInit = true;
                    }
                }

                if (!pipeInit) {
                    // При переключении в неблокирующий режим
                    // произошла ошибка, очистка ресурсов
                    close(fd[0]);
                    close(fd[1]);
                }
            }

            if (!pipeInit) {
                // Ошибка инициализации канала
                pthread_mutex_destroy(&workerPipe->writeLock);
                free(workerPipe);
                workerPipe = NULL;
            }
        } else {
            free(workerPipe);
            workerPipe = NULL;
        }
    }

    return workerPipe;
}

void WorkerPipeFree(worker_pipe_t *workerPipe)
{
    if (workerPipe) {
        close(workerPipe->readFd);
        close(workerPipe->writeFd);
        pthread_mutex_destroy(&workerPipe->writeLock);
    }
}

bool WorkerPipeRead(worker_pipe_t *workerPipe, pthread_t *thread)
{
    bool isOk = false;

    if (workerPipe && thread) {
        void *readData = &workerPipe->readThread;   // Буфер чтения
        const size_t readNeed = sizeof(pthread_t);  // Требуемое количество байт для считывания

        // Функция read не гарантирует чтение необходимого количества байт,
        // для корректного чтения необходим циклический запуск функции
        // до тех пор, как нужное количество байт данных не будет прочитано
        ssize_t readSize = read(workerPipe->readFd, readData + workerPipe->readSize, readNeed - workerPipe->readSize);

        if (readSize > 0) {
            workerPipe->readSize += readSize;

            if (workerPipe->readSize >= readNeed) {
                // Блок данных принят
                workerPipe->readSize -= readNeed;
                *thread = workerPipe->readThread;
                isOk = true;
            }
        } else {
            // Ошибка чтения из канала
        }
    }

    return isOk;
}

bool WorkerPipeWrite(worker_pipe_t *workerPipe, pthread_t thread)
{
    bool isOk = false;

    if (workerPipe) {
        if (pthread_mutex_lock(&workerPipe->writeLock) == 0) {
            WriteAll(workerPipe->writeFd, &thread, sizeof(pthread_t));
            pthread_mutex_unlock(&workerPipe->writeLock);
            isOk = true;
        }
    }

    return isOk;
}

worker_arg_t * WorkerArgInit(worker_pipe_t *workerPipe, const char *srvAddr, const struct sockaddr_in *clientAddr, const char *fileName, tftp_mode_t fileMode)
{
    worker_arg_t *arg = NULL;

    if (workerPipe && srvAddr && clientAddr && fileName) {
        // Разметка памяти для копии аргументов
        char *bufSrvAddr = malloc(strlen(srvAddr) + 1);
        char *bufFileName = malloc(strlen(fileName) + 1);
        arg = malloc(sizeof(worker_arg_t));

        if (bufSrvAddr && bufFileName && arg) {
            strcpy(bufSrvAddr, srvAddr);
            strcpy(bufFileName, fileName);

            arg->workerPipe = workerPipe;   // Обратный канал передается по указателю, структура не копируемая
            arg->srvAddr = bufSrvAddr;
            arg->clientAddr = *clientAddr;
            arg->fileName = bufFileName;
            arg->fileMode = fileMode;
        } else {
            // Ошибка разметки памяти, очистка
            free(bufSrvAddr);
            free(bufFileName);
            free(arg);
            arg = NULL;
        }
    }

    return arg;
}

void WorkerArgFree(worker_arg_t *arg)
{
    if (arg) {
        free(arg->srvAddr);
        free(arg->fileName);
        free(arg);
    }
}

bool WorkerCreateRead(worker_pipe_t *workerPipe, pthread_t *thread, const char *srvAddr, const struct sockaddr_in *clientAddr, const char *fileName, tftp_mode_t fileMode)
{
    bool isOk = false;

    worker_arg_t *arg = WorkerArgInit(workerPipe, srvAddr, clientAddr, fileName, fileMode);

    if (arg) {
        if (pthread_create(thread, NULL, WorkerRead, arg) == 0) {
            isOk = true;
        } else {
            WorkerArgFree(arg);
        }
    }

    return isOk;
}

bool WorkerCreateWrite(worker_pipe_t *workerPipe, pthread_t *thread, const char *srvAddr, const struct sockaddr_in *clientAddr, const char *fileName, tftp_mode_t fileMode)
{
    bool isOk = false;

    worker_arg_t *arg = WorkerArgInit(workerPipe, srvAddr, clientAddr, fileName, fileMode);

    if (arg) {
        if (pthread_create(thread, NULL, WorkerWrite, arg) == 0) {
            isOk = true;
        } else {
            WorkerArgFree(arg);
        }
    }

    return isOk;
}
