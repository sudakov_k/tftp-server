#ifndef SERVER_WORKER_RW_H
#define SERVER_WORKER_RW_H

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Обработка чтения файла.
//! \param vArg Аргументы потока.
//! \return
//!
//! Оригинальный указатель vArg должен иметь тип [worker_arg_t *].
//! По завершению работы ресурсы vArg будут очищены.
//!
void * WorkerRead(void * vArg);

//!
//! \brief Обработка записи файла.
//! \param vArg Аргументы потока.
//! \return
//!
//! Оригинальный указатель vArg должен иметь тип [worker_arg_t *].
//! По завершению работы ресурсы vArg будут очищены.
//!
void * WorkerWrite(void * vArg);

#ifdef __cplusplus
}
#endif

#endif // SERVER_WORKER_RW_H
