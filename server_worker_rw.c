#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include "server_worker.h"
#include "server_worker_rw.h"
#include "server_transfer.h"

void * WorkerRead(void *vArg)
{
    int exitCode = -1;

    if (vArg) {
        worker_arg_t *arg = (worker_arg_t *) vArg;

        // Порт равный 0 означает, что он будет выбран автоматически
        int clientSocket = ServerBindUDP(arg->srvAddr, 0);

        if (clientSocket != -1) {
            // TODO Размер буфера приемника.
            const size_t recvMax = 1024;
            char recvBuf[1024];

            // Максимальный размер блока данных сообщения:
            // ACK - 2 байта,
            // Номер блока - 2 байта,
            // Data - 512 байт,
            const size_t sendMax = 4 + 512;
            char sendBuf[4 + 512];
            const size_t readMax = sendMax - 4;
            char *readBuf = sendBuf + 4;

            // Открыть файл, в зависимости от типа сообщения - в текстовом режиме или в двоичном.
            FILE *file = fopen(arg->fileName, (arg->fileMode == TFTP_MODE_NETASCII) ? "r" : "rb");

            if (file) {
                // TODO Необходимо выполнить проверку на размер файла, но она корректна только для двоичного режима передачи.

                uint32_t msgId = 1;

                bool isExit;
                do {
                    isExit = true;

                    // Чтение файла
                    size_t readSize = fread(readBuf, 1, readMax * sizeof(char), file);

                    // Достижение конца файла не вызовет ошибки
                    if (!ferror(file)) {
                        // Если до конца файла осталось N байт
                        // и выполнено чтение N байт, то флаг EOF
                        // не будет установлен, он будет установлен
                        // при следующем чтении, а размер считанных данных
                        // будет равен 0.

                        if ((readSize == readMax * sizeof(char)) || feof(file)) {
                            size_t sendNeed = TftpData(sendBuf, sendMax, msgId, NULL, readSize);

                            for (int i = 0; i < 3; ++i) {
                                ssize_t sendSize = SendTo(clientSocket, sendBuf, 4 + readSize, 0,
                                                          (const struct sockaddr *) &arg->clientAddr, sizeof(struct sockaddr_in), -1);

                                if ((sendSize >= 0) && (((size_t) sendSize) == sendNeed)) {
                                    // Прием сообщения
                                    // TODO Таймаут
                                    ssize_t recvSize = RecvFrom(clientSocket, recvBuf, recvMax * sizeof(char), 0,
                                                            NULL, 0, 10000);

                                    if (recvSize > 0) {
                                        // Сообщение принято, разбор сообщения
                                        tftp_message_t msg;

                                        if (TftpInfo(recvBuf, recvSize, &msg)) {
                                            if (msg == TFTP_ACK) {
                                                // Обработка ACK.
                                                uint16_t recvMsgId;

                                                if (TftpParseAck(recvBuf, recvSize, &recvMsgId)) {
                                                    if (recvMsgId == msgId) {
                                                        // Получено подтверждение приема
                                                        // Необходимо послать следующую посылку
                                                        ++msgId;

                                                        // Если возможно послать данные
                                                        // и конец файла не был достигнут,
                                                        // продолжаем передачу
                                                        if (msgId <= UINT16_MAX) {
                                                            if (readSize == readMax) {
                                                                isExit = false;
                                                            }
                                                        } else {
                                                            // Ошибка: размер файла слишком большой
                                                            sendNeed = TftpErr(sendBuf, sendMax, TFTP_ERROR_ALLOC, "big file size");

                                                            if (sendNeed <= sendMax) {
                                                                SendTo(clientSocket, sendBuf, sendNeed, 0,
                                                                       (const struct sockaddr *) &arg->clientAddr, sizeof(struct sockaddr_in), -1);
                                                            }
                                                        }

                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    } else if (recvSize == -1) {
                                        // Ошибка или прерывание
                                        break;
                                    }
                                    // Таймаут не обрабатывается, повторная посылка
                                } else {
                                    break;
                                }
                            } /* for (int i = 0; i < 3; ++i) */
                        } else {
                            // Количество считанных байт меньше запрошенного,
                            // но не достигнут конец файла и не установлен флаг ошибки.
                            // Такое поведение не должно происходить.
                        }
                    } else {
                        // Обработка ошибки чтения файла.
                        // При чтении файла произошла ошибка
                        size_t sendNeed = TftpErr(sendBuf, sendMax, TFTP_ERROR_NDEF, "read file");

                        if (sendNeed <= sendMax) {
                            SendTo(clientSocket, sendBuf, sendNeed, 0,
                                   (const struct sockaddr *) &arg->clientAddr, sizeof(struct sockaddr_in), -1);
                        }
                    }
                } while (!isExit);

                fclose(file);
            } else {
                // Ошибка открытия файла, код ошибки в errno.
                size_t sendNeed = 0;

                switch (errno) {
                case ENOENT:
                    sendNeed = TftpErr(sendBuf, sendMax, TFTP_ERROR_NFOUND, "file not found");
                    break;

                case EACCES:
                    sendNeed = TftpErr(sendBuf, sendMax, TFTP_ERROR_ACCESS, "access violation");
                    break;

                default:
                    sendNeed = TftpErr(sendBuf, sendMax, TFTP_ERROR_NDEF, "unknown open file error");
                    break;
                }

                if (sendNeed <= sendMax) {
                    SendTo(clientSocket, sendBuf, sendNeed, 0,
                           (const struct sockaddr *) &arg->clientAddr, sizeof(struct sockaddr_in), -1);
                }
            }

            close(clientSocket);
        } else {
            printf("error: create or bind client socket, errno: %d\n", errno);
        }

        // Передача главному потоку информации о завершающимся потоке
        WorkerPipeWrite(arg->workerPipe, pthread_self());

        // Очистка ресурсов
        WorkerArgFree(arg);

        exitCode = 0;
    }

    return (void *) (size_t) exitCode;
}

void * WorkerWrite(void *vArg)
{
    int exitCode = -1;

    if (vArg) {
        worker_arg_t *arg = (worker_arg_t *) vArg;

        // Порт равный 0 означает, что он будет выбран автоматически
        int clientSocket = ServerBindUDP(arg->srvAddr, 0);

        if (clientSocket != -1) {
            // TODO Размер буфера передатчика.
            const size_t sendMax = 1024;
            char sendBuf[1024];

            // Максимальный размер блока данных сообщения:
            // ACK - 2 байта,
            // Номер блока - 2 байта,
            // Data - 512 байт,
            const size_t recvMax = 4 + 512;
            char recvBuf[4 + 512];

            // Открыть файл, в зависимости от типа сообщения - в текстовом режиме или в двоичном.
            FILE *file = fopen(arg->fileName, (arg->fileMode == TFTP_MODE_NETASCII) ? "w" : "wb");

            if (file) {
                uint32_t msgId = 0;
                bool isExit;
                do {
                    isExit = true;

                    // ACK
                    size_t sendNeed = TftpAck(sendBuf, sendMax, msgId);

                    if (sendNeed <= sendMax) {
                        for (int i = 0; i < 3; ++i) {
                            // Пересылка ACK
                            ssize_t sendSize = SendTo(clientSocket, sendBuf, sendNeed, 0,
                                                      (const struct sockaddr *) &arg->clientAddr, sizeof(struct sockaddr_in), -1);

                            if ((sendSize > 0) && (((size_t) sendSize) == sendNeed)) {
                                // Прием блока
                                // TODO Таймаут
                                ssize_t recvSize = RecvFrom(clientSocket, recvBuf, recvMax, 0,
                                                            NULL, 0, 10000);

                                if (recvSize > 0) {
                                    tftp_message_t msg;

                                    // Сообщение принято, ожидается что сообщение содержит блок с данными
                                    if (TftpInfo(recvBuf, recvSize, &msg)) {
                                        if (msg == TFTP_DATA) {
                                            const char *writeBuf;
                                            size_t writeNeed;
                                            uint16_t recvId;

                                            if (TftpParseData(recvBuf, recvSize, &recvId, &writeBuf, &writeNeed)) {
                                                if (recvId == (msgId + 1)) {
                                                    // Запись блока
                                                    size_t writeSize = fwrite(writeBuf, 1, writeNeed, file);

                                                    if (writeSize == writeNeed) {
                                                        ++msgId;

                                                        if (writeSize < 512) {
                                                            // Подтверждение последнего пакета
                                                            sendNeed = TftpAck(sendBuf, sendMax, msgId);

                                                            if (sendNeed <= sendMax) {
                                                                SendTo(clientSocket, sendBuf, sendNeed, 0,
                                                                       (const struct sockaddr *) &arg->clientAddr, sizeof(struct sockaddr_in), -1);
                                                            }
                                                        } else {
                                                            isExit = false;
                                                        }
                                                    } else {
                                                        // Ошибка записи файла
                                                        sendNeed = TftpErr(sendBuf, sendMax, TFTP_ERROR_NDEF, "write file");

                                                        if (sendNeed <= sendMax) {
                                                            SendTo(clientSocket, sendBuf, sendNeed, 0,
                                                                   (const struct sockaddr *) &arg->clientAddr, sizeof(struct sockaddr_in), -1);
                                                        }
                                                    }

                                                    break;
                                                } else {
                                                    // Неправильный номер блока
                                                }
                                            }
                                        }
                                    }
                                } else if (recvSize == -1) {
                                    // Ошибка или прерывание
                                    break;
                                }

                                // Таймаут
                            } else {
                                // Проблема записи в сокет
                                break;
                            }
                        }
                    }
                } while (!isExit);

                fclose(file);
            } else {
                // Ошибка открытия файла, код ошибки в errno.
                size_t sendNeed = 0;

                switch (errno) {
                case EACCES:
                    sendNeed = TftpErr(sendBuf, sendMax, TFTP_ERROR_ACCESS, "access violation");
                    break;

                default:
                    sendNeed = TftpErr(sendBuf, sendMax, TFTP_ERROR_NDEF, "unknown open file error");
                    break;
                }

                if (sendNeed <= sendMax) {
                    SendTo(clientSocket, sendBuf, sendNeed, 0,
                           (const struct sockaddr *) &arg->clientAddr, sizeof(struct sockaddr_in), -1);
                }
            }

            close(clientSocket);
        } else {
            printf("error: create or bind client socket, errno: %d\n", errno);
        }

        // Передача главному потоку информации о завершающимся потоке
        WorkerPipeWrite(arg->workerPipe, pthread_self());

        // Очистка ресурсов
        WorkerArgFree(arg);

        exitCode = 0;
    }


    return (void *) (size_t) exitCode;
}
