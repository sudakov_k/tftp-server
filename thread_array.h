#ifndef THREAD_ARRAY_H
#define THREAD_ARRAY_H

#include <stdbool.h>
#include <stddef.h>
#include <pthread.h>

//!
//! \brief Массив идентефикаторов потоков.
//!
typedef struct {
    pthread_t *data;    //!< Данные
    size_t size;        //!< Размер массива
    size_t maxSize;     //!< Максимальный размер
} thread_array_t;

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Создает массив идентефикаторов потоков.
//! \param maxSize Максимальный размер массива.
//! \return Указатель на инициализированную структуру или NULL.
//!
thread_array_t * ThreadArrayInit(size_t maxSize);

//!
//! \brief Освобождает ресурсы массива.
//! \param array Массив, созданный функцией ThreadArrayInit.
//!
void ThreadArrayFree(thread_array_t *array);

//!
//! \brief Возвращает размер массива.
//! \param array Массив.
//! \return Размер массива.
//!
//! Если массив имеет нулевой указатель, функция вернет 0.
//!
size_t ThreadArraySize(thread_array_t *array);

//!
//! \brief Возвращает заполненость массива.
//! \param array Массив.
//! \return Степень заполнености.
//!
//! Возвращает true, если масив заполнен
//! или массив имеет нулевой указатель.
//!
bool ThreadArrayFull(thread_array_t *array);

//!
//! \brief Возвращает указанный элемент массива.
//! \param i Массив.
//! \param i Номер элемента.
//! \return Значение элемента.
//!
//! Если i выходит за диапазон доступных значений
//! или массив имеет нулевой указатель, возвращается 0.
//!
pthread_t ThreadArrayData(thread_array_t *array, size_t i);

//!
//! \brief Добавляет значение в конец массива.
//! \param array Массив.
//! \param value Значение, которое необходимо скопировать.
//! \return Успех операции.
//!
//! Операция завершается неуспешно, если массив заполнен
//! или массив имеет нулевой указатель.
//!
bool ThreadArrayAppend(thread_array_t *array, pthread_t value);

//!
//! \brief Удаляет первое найденое значение из массива.
//! \param array Массив.
//! \param value Значение, которое необходимо удалить.
//! \return Успех операции.
//!
//! Операция завершается неуспешно, если значение не найдено
//! или массив имеет нулевой указатель.
//!
bool ThreadArrayRemove(thread_array_t *array, pthread_t value);

#ifdef __cplusplus
}
#endif

#endif // THREAD_ARRAY_H
