#ifndef SERVER_WORKER_H
#define SERVER_WORKER_H

#include <stdbool.h>
#include <stddef.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "protocol.h"

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Канал для передачи сообщений от дочерних потоков к главному.
//!
typedef struct {
    pthread_mutex_t writeLock;  //!< Мьютекс для записи в канал.
    int writeFd;                //!< Файловый дескриптор для записи в канал.

    int readFd;                 //!< Файловый дескриптор чтения из канала.
    size_t readSize;            //!< Количество прочитанных байт.
    pthread_t readThread;       //!< Считываемый поток.
} worker_pipe_t;

//!
//! \brief Аргументы потока.
//!
typedef struct {
    worker_pipe_t *workerPipe;          //!< Обратный канал.
    char *srvAddr;                      //!< Адрес интерфейса (сервер).
    struct sockaddr_in clientAddr;      //!< Адрес клиента.
    char* fileName;                     //!< Имя файла для чтения / записи.
    tftp_mode_t fileMode;               //!< Режим обращения к файлу (двоичный, текстовый).
} worker_arg_t;

//!
//! \brief Инициализация канала обмена.
//! \return
//!
worker_pipe_t * WorkerPipeInit();

//!
//! \brief Освобождение ресурсов канала обмена.
//! \param workerPipe Канал обмена.
//!
void WorkerPipeFree(worker_pipe_t *workerPipe);

//!
//! \brief Чтение из канала обмена.
//! \param workerPipe Канал обмена.
//! \param thread Считанный поток.
//! \return Успех чтения.
//!
//! Данная функция предназначена для запуска из одного и того же потока.
//!
bool WorkerPipeRead(worker_pipe_t *workerPipe, pthread_t *thread);

//!
//! \brief Запись идентефикатора потока в канал обмена.
//! \param workerPipe Канал обмена.
//! \param thread Идентефикатор потока.
//! \return Успех записи.
//!
//! Данная функция предназначена для запуска из дочерних потоков.
//!
bool WorkerPipeWrite(worker_pipe_t *workerPipe, pthread_t thread);

//!
//! \brief Инициализация структуры для передачи обработчику чтения / записи.
//! \param workerPipe Обратный канал.
//! \param srvAddr Адрес интерфейса (сервер).
//! \param clientAddr Адрес клиента.
//! \param fileName Имя файла для чтения / записи.
//! \param fileMode Режим обращения к файлу (двоичный, текстовый).
//! \return
//!
worker_arg_t * WorkerArgInit(worker_pipe_t *workerPipe, const char *srvAddr, const struct sockaddr_in *clientAddr, const char *fileName, tftp_mode_t fileMode);

//!
//! \brief Освобождение структуры, созданной WorkerArgInit.
//! \param arg Освобождаемая структура.
//!
void WorkerArgFree(worker_arg_t *arg);

//!
//! \brief Создает поток для чтения файла.
//! \param workerPipe Обратный канал.
//! \param thread Идентефикатор потока.
//! \param srvAddr Адрес интерфейса (сервер).
//! \param clientAddr Адрес клиента.
//! \param fileName Имя файла.
//! \param fileMode Режим обращения к файлу.
//! \return Успех операции.
//!
//! Обратный канал должен быть валиден до завершения работы потока.
//!
bool WorkerCreateRead(worker_pipe_t *workerPipe, pthread_t *thread, const char *srvAddr, const struct sockaddr_in *clientAddr, const char *fileName, tftp_mode_t fileMode);

//!
//! \brief Создает поток для записи файла.
//! \param workerPipe Обратный канал.
//! \param thread Идентефикатор потока.
//! \param srvAddr Адрес интерфейса (сервер).
//! \param clientAddr Адрес клиента.
//! \param fileName Имя файла.
//! \param fileMode Режим обращения к файлу.
//! \return Успех операции.
//!
//! Обратный канал должен быть валиден до завершения работы потока.
//!
bool WorkerCreateWrite(worker_pipe_t *workerPipe, pthread_t *thread, const char *srvAddr, const struct sockaddr_in *clientAddr, const char *fileName, tftp_mode_t fileMode);

#ifdef __cplusplus
}
#endif

#endif // SERVER_WORKER_H
