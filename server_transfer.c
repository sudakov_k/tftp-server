#include <string.h>
#include <unistd.h>
#include <poll.h>
#include <signal.h>
#include <errno.h>
#include "server_transfer.h"

int ServerBindUDP(const char *addr, uint16_t port)
{
    int fd = -1;            // Файловый дескриптор сокета
    struct in_addr inAddr;  // Сетевой адрес в подходящем для функций формате

    if (inet_aton(addr, &inAddr)) {
        // Создание сокета:
        // - протокол - UDP;
        // - дополнительно - сокет будет переведен в неблокирующий режим.
        fd = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, IPPROTO_UDP);
    } else {
        // Ошибка преобразования адреса
        errno = EINVAL;
    }

    if (fd != -1) {
        struct sockaddr_in service;

        memset(&service, 0, sizeof(struct sockaddr_in));
        service.sin_family = AF_INET;
        service.sin_addr = inAddr;
        service.sin_port = htons(port);

        // Привязкка сокета к сетевому интерфейсу
        if (bind(fd, (const struct sockaddr *) &service, sizeof(struct sockaddr_in)) != 0) {
            // Не удалось привязать сокет к интерфейсу,
            // сокет необходимо закрыть
            close(fd);
            fd = -1;
        }
    }

    return fd;
}

ssize_t WriteAll(int fd, const void *buf, size_t size)
{
    size_t writeSize = 0;   // Общий размер записаных данных
    ssize_t itSize;         // Размер записаных данных за итерацию

    do {
        itSize = write(fd, buf + writeSize, size - writeSize);

        if (itSize > 0) {
            writeSize += itSize;
        } else {
            break;
        }
    } while (writeSize < size);

    return writeSize;
}

int SendTo(int fd, const void *buf, size_t bufSize, int flags, const struct sockaddr *addr, socklen_t addrLen, int timeoutMs)
{
    struct pollfd pfd;
    pfd.fd = fd;
    pfd.events = POLLOUT;
    pfd.revents = 0;

    int err = poll(&pfd, 1, timeoutMs);

    if (err > 0) {
        err = sendto(fd, buf, bufSize, flags, addr, addrLen);
    }

    return err;
}

int RecvFrom(int fd, void *buf, size_t bufSize, int flags, struct sockaddr *addr, socklen_t *addrLen, int timeoutMs)
{
    struct pollfd pfd;
    pfd.fd = fd;
    pfd.events = POLLIN;
    pfd.revents = 0;

    int err = poll(&pfd, 1, timeoutMs);

    if (err > 0) {
        err = recvfrom(fd, buf, bufSize, flags, addr, addrLen);
    }

    return err;
}
