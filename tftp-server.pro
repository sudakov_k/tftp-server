TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lpthread

SOURCES += main.c \
    protocol.c \
    server.c \
    signalhandler.c \
    thread_array.c \
    server_transfer.c \
    server_worker.c \
    server_worker_rw.c \
    cmd_arg.c

HEADERS += \
    protocol.h \
    server.h \
    signalhandler.h \
    thread_array.h \
    server_transfer.h \
    server_worker.h \
    server_worker_rw.h \
    cmd_arg.h
