#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <poll.h>
#include "server.h"
#include "server_transfer.h"
#include "server_worker.h"
#include "signalhandler.h"
#include "protocol.h"
#include "thread_array.h"

int ServerStart(const char *addr, uint16_t port)
{
    // Инициализация обратного канала для сообщения главному потоку
    // о дочерних потоках, которые завершили свою работу
    worker_pipe_t *workerPipe = WorkerPipeInit();

    if (!workerPipe) {
        // Ошибка выделения памяти
        printf("error: pipe init\n");
    }

    // Инициализация массива,
    // которых хранит идентефикаторы дочерних потоков
    thread_array_t *threadArray = NULL;

    if (workerPipe) {
        // TODO Максимальное количество потоков.
        threadArray = ThreadArrayInit(32);

        if (!threadArray) {
            // Ошибка инициализации массива
            printf("error: thread array init\n");
        }
    }

    // Инициализация сокета и его привязка к сетевому интерфейсу
    int srvSocket = -1;

    if (workerPipe && threadArray) {
        srvSocket = ServerBindUDP(addr, port);
    }

    if (workerPipe && threadArray && (srvSocket != -1)) {
        struct pollfd pfd[2];

        // TFTP socket
        pfd[0].fd = srvSocket;
        pfd[0].events = POLLIN;

        // pipe
        pfd[1].fd = workerPipe->readFd;
        pfd[1].events = POLLIN;

        // TODO размер буфера
        char buf[1024];
        const size_t bufSize = 1024;

        // Циклический опрос файловых дескрипторов обратного канала и сокета сервера
        // до тех пор пока не будет получен сигнал завершения работы
        for (bool isExit = false; !isExit;) {
            pfd[0].revents = 0;
            pfd[1].revents = 0;

            if (poll(pfd, 2, -1) != -1) {
                // Обработка входящего сообщения
                if (pfd[0].revents & POLLIN) {
                    // Информация о клиенте
                    struct sockaddr_in clientAddr;
                    socklen_t clientAddrSize = sizeof(struct sockaddr_in);
                    memset(&clientAddr, 0, clientAddrSize);

                    // Получение сообщения, в clientAddr будет записан адрес клиента
                    ssize_t recvSize = recvfrom(srvSocket, buf, bufSize * sizeof(char), 0,
                             (struct sockaddr *) &clientAddr, &clientAddrSize);

                    switch (recvSize) {
                    case 0:
                        // shutdown
                        printf("error: server socket shutdown\n");
                        isExit = true;
                        break;

                    case -1:
                        // Ошибка
                        printf("error: server recvfrom, errno: %d (%s)\n", errno, strerror(errno));
                        break;

                    default:
                        // Проверка массива рабочих потоков на заполненость,
                        // если количество рабочих потоков достигло максимума, сообщение игнорируется
                        if (!ThreadArrayFull(threadArray)) {
                            tftp_message_t message;         // Тип запроса от клиента

                            // Проверка типа сообщения
                            if (TftpInfo(buf, recvSize, &message)) {
                                if (message == TFTP_RRQ || message == TFTP_WRQ) {
                                    // Запрос на чтение или запись файла
                                    const char *fileName;   // Имя файла (будет получено из сообщения)
                                    tftp_mode_t fileMode;   // Режим передачи (будет получен из сообщения)

                                    // Разбор сообщения чтения / записи
                                    if (TftpParseRW(buf, recvSize, &fileName, &fileMode)) {
                                        // Сообщение успешно разобрано
                                        // Далее необходимо создать поток для дальнейшей работы с клиентом

                                        bool threadOk;      // Успех создания потока
                                        pthread_t thread;   // Дескриптор потока

                                        if (message == TFTP_RRQ) {
                                            // Дочерний поток - обработка чтения файла
                                            threadOk = WorkerCreateRead(workerPipe, &thread, addr, &clientAddr, fileName, fileMode);
                                        } else {
                                            // Дочерний поток - обработка записи файла
                                            threadOk = WorkerCreateWrite(workerPipe, &thread, addr, &clientAddr, fileName, fileMode);
                                        }

                                        if (threadOk) {
                                            // Поток успешно создан, внесение идентефикатора потока в массив
                                            // Возвращаемое значение функции ThreadArrayAppend - true,
                                            // так как выше была проверка на заполненность массива
                                            ThreadArrayAppend(threadArray, thread);
                                        } else {
                                            // Ошибка создания потока
                                        }
                                    }
                                } else {
                                    // Неправильный запрос,
                                    // на данный порт могут поступать только запросы на чтение и запись файла

                                    printf("error: non RRQ or WRQ to server port\n");
                                }
                            } else {
                                printf("error: invalid message from %s, port %"PRIu16"\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));
                            }
                        } else {
                            // Превышено количество клиентов
                            printf("server recieve message, but threads limit\n");
                        }

                        break;
                    }
                }

                // Обработка дочернего потока
                if (pfd[1].revents & POLLIN) {
                    pthread_t thread;

                    if (WorkerPipeRead(workerPipe, &thread)) {
                        // Поток завершил работу и нужно освободить ресурсы
                        pthread_join(thread, NULL);
                        ThreadArrayRemove(threadArray, thread);
                    }
                }
            } else {
                // Ошибка или сигнал завершения работы
                if (errno == EINTR) {
                    isExit = SignalExit();
                } else {
                    printf("error: server poll, errno: %d (%s)\n", errno, strerror(errno));
                }
            }
        }

        // Завершение дочерних потоков
        for (size_t i = 0; i < ThreadArraySize(threadArray); ++i) {
            pthread_t thread = ThreadArrayData(threadArray, i);

            pthread_kill(thread, SIGTERM);
            pthread_join(thread, NULL);
        }

        close(srvSocket);
    } else {
        printf("error: create or bind server socket, errno: %d\n", errno);
    }

    // Очистка памяти
    ThreadArrayFree(threadArray);
    WorkerPipeFree(workerPipe);

    return 0;
}
