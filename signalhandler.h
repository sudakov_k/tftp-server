#ifndef SIGNALHANDLER_H
#define SIGNALHANDLER_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Установка обработчика сигналов.
//!
void SignalInit();

//!
//! \brief Сигнал завершения работы.
//! \return true - необходимо завершить работу.
//!
bool SignalExit();

#ifdef __cplusplus
}
#endif

#endif // SIGNALHANDLER_H
