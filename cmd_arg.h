#ifndef CMD_ARG_H
#define CMD_ARG_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Параметры сервера.
//!
typedef struct {
    const char *addr;   //!< Адрес сервера.
    uint16_t port;      //!< Порт сервера.
    const char *path;   //!< Рабочий каталог.
} cmd_arg_t;

//!
//! \brief Заполнить структуру arg стандартными значениями.
//! \param arg Структура с параметрами.
//!
void CmdArgDefault(cmd_arg_t *arg);

//!
//! \brief Напечатать справку.
//!
void CmdArgHelp(const char *progName);

//!
//! \brief Разобрать аргументы командной строки.
//! \param arg Структура с параметрами.
//! \param argc Аргументы командной строки.
//! \param argv Количество аргументов командной строки.
//! \param Код выхода.
//! \return Параметры разобраны успешно.
//!
//! При отсутствии параметра будет записано стандартное значение.
//! Если была выбрана рабочая директория, функция сменит текущую рабочую директорию на заданную.
//! Если была запрошена справка, функция вернет false и установит код выхода равный 0.
//!
bool CmdArgParse(cmd_arg_t *arg, int argc, char * const* argv, int* exitCode);

#ifdef __cplusplus
}
#endif

#endif // CMD_ARG_H
