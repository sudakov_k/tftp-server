#include <stddef.h>
#include <stdio.h>
#include <inttypes.h>
#include <getopt.h>
#include <unistd.h>
#include <errno.h>
#include "cmd_arg.h"

void CmdArgDefault(cmd_arg_t *arg)
{
    if (arg) {
        arg->addr = "0.0.0.0";  // Стандартный сетевой интерфейс - любой
        arg->port = 69;         // Стандартный порт TFTP
        arg->path = NULL;       // NULL - не менять рабочий каталог
    }
}

void CmdArgHelp(const char *progName)
{
    printf( \
                "%s [args]\n" \
                "args:\n" \
                "-h, --help           : help;\n" \
                "-a ADDR, --addr ADDR : interface address, default: \"0.0.0.0\";\n" \
                "-p PORT, --port PORT : port, default: \"69\";\n" \
                "-P PATH, --path PATH : work directory, default: \".\".\n", progName);
}

bool CmdArgParse(cmd_arg_t *arg, int argc, char * const* argv, int* exitCode)
{
    bool isOk = false;

    if (arg) {
        CmdArgDefault(arg);

        const struct option longOpt[] = {
            { "help", no_argument, NULL, 'h' },
            { "addr", required_argument, NULL, 'a' },
            { "port", required_argument, NULL, 'p' },
            { "path", required_argument, NULL, 'P' },
            { 0, 0, 0, 0 }
        };

        int code = 0;
        int shortOpt;
        isOk = true;

        // Разбор параметров командной строки
        while (((shortOpt = getopt_long(argc, argv, "ha:p:P:", longOpt, NULL)) != -1) && isOk) {
            switch (shortOpt) {

            // Справка; если указан данный ключ,
            // будет выведена информация и программа завершит работу
            case 'h':
                CmdArgHelp(argv[0]);
                /* code = 0; */
                isOk = false;
                break;

            // Задание адреса интерфейса
            case 'a':
                arg->addr = optarg;
                break;

            // Задание порта
            case 'p':
                if (sscanf(optarg, "%"SCNu16, &arg->port) != 1) {
                    printf("error: \"%s\" is not port\n", optarg);
                    code = -1;
                    isOk = false;
                }

                break;

            // Задание рабочего каталога
            case 'P':
                arg->path = optarg;
                break;

            // Неизвестные ключи, завершение работы программы
            /* case '?': */
            default:
                CmdArgHelp(argv[0]);
                code = -1;
                isOk = false;
                break;
            }

        }

        // Смена рабочей директории
        if (isOk && arg->path) {
            if (chdir(arg->path) == 0) {
                printf("change directory \"%s\"\n", arg->path);
            } else {
                printf("error: can't change directory \"%s\", errno=%d\n", arg->path, errno);
                code = -1;
                isOk = false;
            }
        }

        if (exitCode) {
            *exitCode = code;
        }
    }

    return isOk;
}
