#ifndef SERVER_TRANSFER_H
#define SERVER_TRANSFER_H

#include <stddef.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Создание UDP сокета с привязкой к сетевому интерфейсу.
//! \param addr Адрес сетевого интерфейса.
//! \param port Порт.
//! \return Файловый дескриптор сокета или -1 в случае неудачи.
//!
//! Сокет будет создан с флагом SOCK_NONBLOCK.
//! В случае неудачи в errno будет записан код ошибки.
//!
int ServerBindUDP(const char *addr, uint16_t port);

//!
//! \brief Запись данных в файл для блокирующих файлов.
//! \param fd Файловый дескриптор.
//! \param buf Буфер.
//! \param size Размер буфера.
//! \return Количество успешно записанных данных.
//!
//! Циклический вызов функции write, пока все данные не будут переданы.
//!
ssize_t WriteAll(int fd, const void *buf, size_t size);

//!
//! \brief Функция аналогична sendto для неблокирующих сокетов.
//! \param fd Файловый дескриптор.
//! \param buf Буфер.
//! \param bufSize Размер буфера.
//! \param flags Флаги.
//! \param addr Адрес назначения.
//! \param addrLen Размер структуры адреса.
//! \param timeoutMs Таймаут (мс).
//! \return
//!
int SendTo(int fd, const void *buf, size_t bufSize, int flags, const struct sockaddr *addr, socklen_t addrLen, int timeoutMs);

//!
//! \brief Функция аналогична recvfrom для неблокирующих сокетов.
//! \param fd Файловый дескриптор.
//! \param buf Буфер.
//! \param bufSize Размер буфера.
//! \param flags Флаги.
//! \param addr Адрес передатчика.
//! \param addrLen Размер структуры адреса.
//! \param timeoutMs Таймаут (мс).
//! \return
//!
int RecvFrom(int fd, void *buf, size_t bufSize, int flags, struct sockaddr *addr, socklen_t *addrLen, int timeoutMs);

#ifdef __cplusplus
}
#endif

#endif // SERVER_TRANSFER_H
