#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

//!
//! \brief Тип посылки TFTP.
//!
typedef enum {
    TFTP_RRQ = 1,   //!< Запрос на чтение файла.
    TFTP_WRQ = 2,   //!< Запрос на запись файла.
    TFTP_DATA = 3,  //!< Передача данных.
    TFTP_ACK = 4,   //!< Подтверждение данных.
    TFTP_ERR = 5,   //!< Ошибка.
} tftp_message_t;

//!
//! \brief Режим передачи.
//!
typedef enum {
    TFTP_MODE_NETASCII, //!< Файл кодируется в ASCII.
    TFTP_MODE_OCTET,    //!< Файл передается без изменений.
    TFTP_MODE_MAIL,     //!< Устаревший режим передачи, не используется.
    TFTP_MODE_ERROR,    //!< Ошибка, режима нет в списке доступных.
} tftp_mode_t;

//!
//! \brief Код ошибки.
//!
typedef enum {
    TFTP_ERROR_NDEF = 0,    //!< Не определена, подробности в тексте сообщения (если есть).
    TFTP_ERROR_NFOUND = 1,  //!< Файл не найден.
    TFTP_ERROR_ACCESS = 2,  //!< Доступ запрещен.
    TFTP_ERROR_ALLOC = 3,   //!< Диск переполнен или ошибка аллокации.
    TFTP_ERROR_ILLEGAL = 4, //!< Неправильная операция TFTP.
    TFTP_ERROR_ID = 5,      //!< Неправильный номер блока.
    TFTP_ERROR_FEXIST = 6,  //!< Файл уже существует.
    TFTP_ERROR_USER = 7,    //!< Пользователь не существует.
} tftp_error_t;

#ifdef __cplusplus
extern "C" {
#endif

//!
//! \brief Информация о сообщении.
//! \param buf Буфер сообщения.
//! \param bufSize Размер буфера сообщения.
//! \param message Тип сообщения.
//! \return Успех разбора сообщения.
//!
bool TftpInfo(const char *buf, size_t bufSize, tftp_message_t *message);

//!
//! \brief Разобрать сообщение RRQ и WRQ.
//! \param buf Буфер сообщения.
//! \param bufSize Размер буфера сообщения.
//! \param fileName Указатель на строку с именем файла.
//! \param mode Режим передачи файла.
//! \return Успех разбора сообщения.
//!
//! При успешном разборе в переменннную fileName будет записан указатель на строку с именем файла,
//! этот указатель равен buf + 2.
//!
bool TftpParseRW(const char *buf, size_t bufSize, const char **fileName, tftp_mode_t *mode);

//!
//! \brief Разобрать сообщение ACK.
//! \param buf Буфер сообщения.
//! \param bufSize Размер буфера сообщения.
//! \param i Номер блока.
//! \return Успех разбора сообщения.
//!
//! Если i будет равен NULL,
//! разбор все равно считается успешным.
//!
//! Разбор считается неуспешным,
//! если размер буфера не равен 4 байтам или buf равен NULL.
//!
bool TftpParseAck(const char *buf, size_t bufSize, uint16_t *i);

//!
//! \brief Разобрать сообщение ERR.
//! \param buf Буфер сообщения.
//! \param bufSize Размер буфера сообщения.
//! \param errCode Код ошибки.
//! \param errStr Строка с описанием ошибки.
//! \param strSize Размер буфера строки.
//! \return Успех разбора сообщения.
//!
//! Если в буфере строки файла недостаточно места,
//! сообщение об ошибке не будет записано, в переменной strSize
//! будет возвращен требуемый размер буфера.
//! При этом разбор будет считаться успешным.
//!
//! errCode может быть равным NULL.
//!
bool TftpParseErr(const char *buf, size_t bufSize, tftp_error_t *errCode, char *errStr, size_t *strSize);

//!
//! \brief Разобрать сообщение DATA.
//! \param buf Входной буфер.
//! \param bufSize Размер входного буфера.
//! \param id Номер блока.
//! \param data Указатель на начало блока данных.
//! \param dataSize Размер блока данных.
//! \return Успех разбора сообщения.
//!
//! В переменную дата записывается указатель на начало данных,
//! который равен (buf + 4)
//!
bool TftpParseData(const char *buf, size_t bufSize, uint16_t *id, const char **data, size_t *dataSize);

//!
//! \brief Сформировать сообщение DATA.
//! \param outBuf Выходной буфер.
//! \param outSize Размер выходного буфера.
//! \param i Номер блока.
//! \param inBuf Входной буфер.
//! \param inSize Размер входного буфера.
//! \return Требуемый размер выходного буфера.
//!
//! Если выходной буфер имеет недостаточный размер или равен NULL,
//! буфер не будет перезаписан и функция вернет требуемый размер.
//!
//! Если входной буфер равен NULL, выходной буфер не будет перезаписан,
//! но функция вернет размер с учетом inSize.
//!
//! Функцию можно использовать для вставки типа DATA и номера сообщения
//! в первые 4 байта буфера. Для этого необходимо передать размер выходного буфера равным 4,
//! указать номер сообщения, inBuf установить равным NULL, inSize равным 0.
//!
//! Узнать требуемый размер выходного буфера:
//! size_t size = TftpData(NULL, 0, i, NULL, inSize);
//!
//! Формирование сообщения с подготовленной секцией DATA (реальный размер буфера равен 4 + 512 байт):
//! size_t size = TftpData(outBuf, 4, i, NULL, 0);
//!
size_t TftpData(char *outBuf, size_t outSize, uint16_t i, const char *inBuf, size_t inSize);

//!
//! \brief Сформировать сообщение ACK.
//! \param outBuf Выходной буфер.
//! \param outSize Размер выходного буфера.
//! \param i Номер блока.
//! \return Требуемый размер выходного буфера.
//!
size_t TftpAck(char *outBuf, size_t outSize, uint16_t i);

//!
//! \brief Сформировать сообщение ERR.
//! \param outBuf Выходной буфер.
//! \param outSize Размер выходного буфера.
//! \param err Код ошибки.
//! \param str Строка ошибки.
//! \return Требуемый размер выходного буфера.
//!
size_t TftpErr(char *outBuf, size_t outSize, tftp_error_t err, const char *str);

#ifdef __cplusplus
}
#endif

#endif // PROTOCOL_H
