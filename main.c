#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include "signalhandler.h"
#include "server.h"
#include "cmd_arg.h"

int main(int argc, char *argv[])
{
    // Разбор параметров командной строки
    cmd_arg_t arg;
    int exitCode;

    if (CmdArgParse(&arg, argc, argv, &exitCode)) {
        // Вывод параметров сервера
        printf(
                    "addr: \"%s\"\n" \
                    "port: \"%"PRIu16"\"\n", \
                    arg.addr, arg.port);

        SignalInit();
        exitCode = ServerStart(arg.addr, arg.port);
    }

    return exitCode;
}
