#include <string.h>
#include <signal.h>
#include "signalhandler.h"

static volatile sig_atomic_t isExit = 0;

// Обработчик сигналов
static void SignalHandler(int sigNum)
{
    (void) sigNum;

    isExit = 1;
}

void SignalInit()
{
    struct sigaction sa;
    memset(&sa, 0, sizeof(struct sigaction));

    sa.sa_handler = SignalHandler;
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
}

bool SignalExit()
{
    return isExit != 0;
}
